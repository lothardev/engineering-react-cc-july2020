import React, { useEffect, useState } from 'react';
import cn from 'classnames';

interface TabBarProps {
  items: any[];
  active: any;
  onTabClick: (tab: any) => void;
}

export const TabBar: React.FC<TabBarProps> = (props) => {
  const tabHandler = (tab: any) => {
    props.onTabClick(tab);
  };

  return <ul className="nav nav-tabs">
    {
      props.items.map((item) => {
        return (
          <li
            key={item.id}
            className="nav-item"
            onClick={() => tabHandler(item)}
          >
            <a
              className={cn(
                'nav-link',
                { 'active': item.id === props.active?.id}
              )}

            >
              {item.label}
            </a>
          </li>
        )
      })
    }
  </ul>
}
