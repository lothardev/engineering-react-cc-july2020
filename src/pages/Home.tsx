import React, { Fragment } from 'react';
import { Button, MenuItem } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import { useHistory } from 'react-router-dom';

export default function Home() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const history = useHistory()
  const handleClick = (event: any) => {
    console.log(event.currentTarget)
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (a?: number | string) => {
    console.log(a)
    history.push('users')
    setAnchorEl(null);
  };

  return (
    <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        Open Menu
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => handleClose()}
      >
        <MenuItem onClick={() => handleClose(1)}>Profile</MenuItem>
        <MenuItem onClick={() => handleClose('account')}>My account</MenuItem>
        <MenuItem onClick={() => handleClose()}>Logout</MenuItem>
      </Menu>
    </div>
  )
}
