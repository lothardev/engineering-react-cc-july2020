import React, { useEffect, useState } from 'react';
import { openAlert } from '../utils/alerts';
import { GoogleMap } from '../components/GoogleMap';
import { Card } from '../components/Card';
import { TabBar } from '../components/TabBar';
import Axios from 'axios';

interface Country {
  id: number;
  label: string;
}

export function UIKit() {
  const [activeCountry, setActiveCountry] = useState<Country | null>(null);
  const [result, setResult] = useState<Country[]>([]);

  useEffect(() => {
    Axios.get<Country[]>('http://localhost:3001/countries')
      .then(res => {
        setResult(res.data);
        setActiveCountry(res.data[0]);
      })
  }, [])


  function openLink() {
    window.open('http://www.google.com')
  };

  function doSomething(tab: any) {
    setActiveCountry(tab)
  }

  return (
    <div>
      <TabBar
        onTabClick={doSomething}
        items={result}
        active={activeCountry}
      />

      {
        activeCountry && <GoogleMap city={activeCountry.label} zoom={10} />
      }


      <Card
        title="empty"
        icon="fa fa-link"
        onIconClick={openAlert}
      />
      <Card title="Profile"  >
        <form>
          <input type="text"/>
          <input type="text"/>
          <input type="text"/>
          <input type="text"/>
        </form>
      </Card>
    </div>
  )
}

