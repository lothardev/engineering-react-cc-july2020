import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import Axios from 'axios';
import { User } from '../model/user';
import { Switch } from '@material-ui/core';

interface FormDataProps {
  name: string;
  surname: string;
  isCompany: 'yes' | 'no' | '';
  subscription: boolean;
}

const INITIAL_STATE: FormDataProps = { name: '', surname: '', isCompany: '', subscription: true};

const UsersControlled: React.FC = () => {
  const [formData, setFormData] = useState<FormDataProps>(INITIAL_STATE);
  const [dirty, setDirty] = useState<boolean>(false);
  const [users, setUsers] = useState<User[]>([]);

  useEffect(() => {
    Axios.get<User[]>('http://localhost:3001/users/')
      .then(res => setUsers(res.data))
  }, []);

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const { name, value, type } = e.currentTarget;

    setFormData({
      ...formData,
      [name]: type === 'checkbox' ? (e.currentTarget as HTMLInputElement).checked : value,
    });

    setDirty(true);
  }

  function submitHandler(e: React.FormEvent) {
    e.preventDefault();
    Axios.post<User>('http://localhost:3001/users/', formData)
      .then(res => {
        setUsers([...users, res.data ]);
        setFormData({...INITIAL_STATE})
      })
  }

  function deleteHandler(id: number) {
    Axios.delete('http://localhost:3001/users/' + id)
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        )
      })
  }
  const isNameValid = formData.name.trim().length > 3;
  const isSurnameValid = formData.surname.length
  const isCompanyValid = formData.isCompany !== '';
  const isValid = isNameValid && isSurnameValid && isCompanyValid;

  return <div>
    {JSON.stringify(formData)}
    <form onSubmit={submitHandler}>
      <input
        type="text"
        name="name"
        placeholder="name"
        className={cn(
          'form-control',
          { 'is-valid': isNameValid },
          { 'is-invalid': !isNameValid && dirty},
        )}
        value={formData.name}
        onChange={onChangeHandler}
      />
      <input
        type="text"
        name="surname"
        placeholder="surname"
        className={cn(
          'form-control',
          { 'is-valid': isSurnameValid },
          { 'is-invalid': !isSurnameValid && dirty },
        )}
        value={formData.surname}
        onChange={onChangeHandler}
      />

      <select
        name="isCompany"
        className={cn(
          'form-control',
          { 'is-valid': isCompanyValid },
          { 'is-invalid': !isCompanyValid && dirty },
        )}
        onChange={onChangeHandler}
        value={formData.isCompany}
      >
        <option value={''}>Are you a company</option>
        <option value="yes">YES, i am</option>
        <option value="no">NO, I'm not</option>
      </select>

      <Switch
        checked={formData.subscription}
        onChange={onChangeHandler}
        name="subscription"
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      />

      <button
        type="submit"
        className="btn btn-outline-primary"
        style={{ borderColor: isValid ? 'green' : 'red'}}
        disabled={!isValid}
      >SAVE</button>
    </form>


    <hr/>
    {
      users.map(u => {
        return <li key={u.id} className="list-group-item">
          {u.name} {u.surname} ({u.id})
          <div className="pull-right">
            <i className="fa fa-trash" onClick={() => deleteHandler(u.id)} />
          </div>
        </li>
      })
    }
  </div>
}

export default UsersControlled;
