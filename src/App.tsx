import React, { lazy, useState, Suspense } from 'react';
import DemoState from './pages/DemoState';
import { UIKit } from './pages/UIKit';
import { NavBar } from './components/Nabbar';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import UsersControlled from './pages/UsersControlled';
import { UsersDetails } from './pages/UsersDetails';
const Home = lazy(() => import('./pages/Home'));
const Users = lazy(() => import('./pages/Users'));

function App() {

  return (
    <BrowserRouter >
      <NavBar />

      <Suspense fallback={<div>Loading....</div>}>
        <div className="center">
          <Switch>
            <Route path="/demo-state">
              <DemoState/>
            </Route>
            <Route path="/uikit">
              <UIKit />
            </Route>
            <Route path="/users">
              <Users />
            </Route>

            <Route path="/users-controlled">
              <UsersControlled />
            </Route>

            <Route
              path="/users-details/:id"
              component={UsersDetails}
            />

            <Route path="/" exact={true}>
              <Home />
            </Route>
            <Route path="*">
              <Redirect to="/" />
            </Route>
          </Switch>
        </div>
      </Suspense>

    </BrowserRouter>
  )
}

export default App;
